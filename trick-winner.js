const trickDecree = require("./trick-decree");

module.exports = function trickWinner(Trick) {
  const { North, South } = Trick;
  if (North.Rank !== undefined && North.Suit !== undefined && South.Rank !== undefined && South.Suit !== undefined) {

    const cards = [North, South];
    const foxCount = cards.filter(c => c.Rank === 3).length;
    const woodcutterCount = cards.filter(c => c.Rank === 5).length;
    if (Trick.Fox.length < foxCount) {
      return;
    }
    if (Trick.Woodcutter.length < woodcutterCount) {
      return;
    }

    const Decree = trickDecree(Trick);

    const NorthTrump = North.Suit === Decree.Suit || North.Rank === 9 && South.Rank !== 9;
    const SouthTrump = South.Suit === Decree.Suit || South.Rank === 9 && North.Rank !== 9;

    if (NorthTrump && ! SouthTrump) {
      return "North";
    }
    if (SouthTrump && ! NorthTrump) {
      return "South";
    }
    if (North.Suit !== South.Suit) {
      return Trick.Lead;
    }
    return North.Rank > South.Rank ? "North" : "South";

  } 

}
