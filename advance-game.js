const crypto = require("crypto");
const base64url = require("base64url");

const Trick = require("./trick.model");
const Round = require("./round.model");

const trickWinner = require("./trick-winner");
const opposite = require("./opposite");
const scoreRound = require("./score-round");
const dealRound = require("./deal-round");
const trickDecree = require("./trick-decree");

module.exports = function advanceGame(game) {
  const { Length } = game;
  const round = game.Rounds.slice(-1)[0];
  const trick = round.Tricks.slice(-1)[0];
  const Winner = trickWinner(trick);
  if (Winner) {
    if (round.Tricks.length >= 13) {
      const Scores = {North: 0, South: 0};
      game.Rounds.forEach(round => {
        const roundScore = scoreRound(round);
        Scores.North += roundScore.North;
        Scores.South += roundScore.South;
      });
      if ((Scores.North > Length || Scores.South > Length) && Scores.North !== Scores.South) {
        game.Winner = Scores.North > Scores.South ? "North" : "South";
      } else {
        const newRound = new Round();
        newRound.Index = round.Index + 1;
        newRound.Dealer = opposite(round.Dealer);
        newRound.Seed = base64url(crypto.randomBytes(64));
        
        const deal = dealRound(newRound);
        newTrick = new Trick();
        newTrick.Index = 1;
        newTrick.Lead = round.Dealer;
        newTrick.Decree = deal.Decree;
        newRound.Tricks.push(newTrick);
        game.Rounds.push(newRound);
      }
    } else {
      const Loser = opposite(Winner);
      const newTrick = new Trick();
      newTrick.Index = trick.Index + 1;
      newTrick.Lead = Winner;
      newTrick.Decree = trickDecree(trick);
      if (trick[Winner].Rank !== 1 && trick[Loser].Rank === 1) {
        newTrick.Lead = Loser;
      }
      round.Tricks.push(newTrick);
    }
  }
  
  game.UpdatedAt = new Date();
  return game;
}
