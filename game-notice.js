const trickTurn = require("./trick-turn");
const trickSpecial = require("./trick-special");
const trickWinner = require("./trick-winner");
const scoreRound = require("./score-round");
const opposite = require("./opposite");

module.exports = function gameNotice(game) {

  //list the last thing to happen, and the next thing that should happen
  let last = "", next = "";

  const Round = game.Rounds.slice(-1)[0];
  const Trick = Round.Tricks.slice(-1)[0];
  const LastTrick = Round.Tricks.slice(-2)[0];
  if (Round.Tricks.length > 1) {
    const lastWinner = trickWinner(LastTrick);
    last = `${lastWinner} won the last trick with ${LastTrick[lastWinner].Rank} ${LastTrick[lastWinner].Suit}. `;
  } else if (Round.Index > 1) {
    const lastRound = game.Rounds.slice(-2)[0];
    const { Message } = scoreRound(lastRound);
    last = Message;
  }

  const { Lead } = Trick;
  const Follow = opposite(Lead);
  const Turn = trickTurn(Trick);
  const Special = trickSpecial(Trick);

  const verb = (Turn === Lead ) ? "lead" : "follow";
  next = `${Turn} to ${verb}`;

  if (Special !== undefined) {
    switch (Special) {
      case "Fox":
        next = `${Turn} may change Decree`;
        break;

      case "Woodcutter":
        next = `${Turn} must choose a card to Discard`;
        break;
    }
  }

  return last + "\n" + next;
}
