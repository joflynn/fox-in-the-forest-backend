const seedDeck = require("./deck");

function removeCard(hand, card) {
  const idx = hand.findIndex(c => c.Rank === card.Rank && c.Suit === card.Suit);
  if (idx > -1) {
    hand.splice(idx, 1);
  }
}

function playTrick(trick, leadHand, followHand, draw) {
  const Follow = (trick.Lead === "North") ? "South" : "North";
  const leadCard = trick[trick.Lead];

  let foxIndex = 0;
  let woodcutterIndex = 0;

  removeCard(leadHand, leadCard);
  if (leadCard.Rank === 3) {
    leadHand.push(trick.Decree);
    if (trick.Fox.length > foxIndex) {
      const { NewDecree } = trick.Fox[foxIndex];
      removeCard(leadHand, NewDecree);
      foxIndex += 1;
    }
  }

  if (leadCard.Rank === 5) {
    const drawCard = draw();
    leadHand.push(drawCard);
    if (trick.Woodcutter.length > woodcutterIndex) {
      const { Discard } = trick.Woodcutter[woodcutterIndex];
      removeCard(leadHand, Discard);
      woodcutterIndex += 1;
    }
  }

  const followCard = trick[Follow];
  removeCard(followHand, followCard);
  if (followCard.Rank === 3) {
    followHand.push(trick.Decree);
    if (trick.Fox.length > foxIndex) {
      const { NewDecree } = trick.Fox[foxIndex];
      removeCard(followHand, NewDecree);
    }
  }

  if (followCard.Rank === 5) {
    const drawCard = draw();
    followHand.push(drawCard);
    if (trick.Woodcutter.length > woodcutterIndex) {
      const { Discard } = trick.Woodcutter[woodcutterIndex];
      removeCard(followHand, Discard);
    }
  }
}

module.exports = function dealRound(round, side) {

  const Deck = seedDeck(round.Seed);

  function draw() {
    return Deck.shift();
  }

  const North = [];
  const South = [];

  for (let i = 0; i < 13; i += 1) {
    switch (round.Dealer) {
      case "North":
        South.push(draw());
        North.push(draw());
        break;

      case "South":
        North.push(draw());
        South.push(draw());
        break;

      default:
        break;
    }
  }

  let Decree = Deck.shift();

  round.Tricks.forEach(trick => {
    const { Lead } = trick;
    const Follow = (Lead === "North") ? "South" : "North";
    if (trick.Decree === undefined) {
      trick.Decree = Decree 
    }

    if (Lead === "North") {
      const LeadHand = North;
      const FollowHand = South;
      playTrick(trick, LeadHand, FollowHand, draw);
    } else {
      const LeadHand = South;
      const FollowHand = North;
      playTrick(trick, LeadHand, FollowHand, draw);
    }

    if ( trick.Fox.length > 0) {
      Decree = trick.Fox.slice(-1)[0].NewDecree;
    }
  });

  let Hand = undefined;
  if (side === "North") {
    Hand = North;
  }
  if (side === "South") {
    Hand = South;
  }


  return { Hand, Decree, draw };
}
