const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Round = new Schema({
  _game: {
    type: Schema.Types.ObjectId, ref: 'Game'
  },
  Index: {
    type: Number
  },
  Dealer: {
    type: String
  },
  Seed: {
    type: String
  },
  Tricks: [{
    Index: Number,
    Lead: String,
    Decree: {Rank: Number, Suit: String},
    North: {Rank: Number, Suit: String},
    South: {Rank: Number, Suit: String},
    Fox: [{NewDecree: {Rank: Number, Suit: String}}],
    Woodcutter: [{Discard: {Rank: Number, Suit: String}}]
  }]
});

module.exports = mongoose.model("Round", Round);
