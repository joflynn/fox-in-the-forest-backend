const handVoid = require("./hand-void");
module.exports = function monarchRequires(Hand, Lead) {
  if (Lead.Rank === 11 && !handVoid(Hand, Lead)) {
    const swanIdx = Hand.findIndex(card => card.Suit === Lead.Suit && card.Rank === 1);
    if (swanIdx != -1) {
      return Hand[swanIdx];
    }
    return Hand.reduce((acc, cur) => {
      if (cur.Suit === Lead.Suit && (acc === undefined || acc.Rank < cur.Rank )) {
        return cur;
      }
      return acc;
    }, undefined);
    
  }
}
