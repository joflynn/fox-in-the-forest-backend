const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Game = new Schema({
  Name: String,
  Length: String,
  North: String,
  South: String,
  Invite: String,
  CreatedAt: Date,
  UpdatedAt: Date,
  Winner: String,
  Rounds: [{
    Index: Number,
    Dealer: String,
    Seed: String,
    Tricks: [{
      Index: Number,
      Lead: String,
      Decree: {Rank: Number, Suit: String},
      North: {Rank: Number, Suit: String}, 
      South: {Rank: Number, Suit: String},
      Fox: [{NewDecree: {Rank: Number, Suit: String}}],
      Woodcutter: [{Discard: {Rank: Number, Suit: String}}] 
    }]
  }]

});

module.exports = mongoose.model("Game", Game);
