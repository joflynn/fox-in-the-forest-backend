const shuffleSeed = require("shuffle-seed");

module.exports = function seedDeck(seed) {
  const suits = ["Bells", "Moons", "Keys"];
  const cards = Array.from({length: 33}, (v, i) => {
    return {
      Rank: i % 11 + 1, 
      Suit: suits[Math.floor(i / 11)]
    }
  });

  return shuffleSeed.shuffle(cards, seed);
}
