const opposite = require("./opposite");

module.exports = function trickSpecial(Trick) {
  const { Lead } = Trick;
  const Fox = Trick.Fox || [];
  const Woodcutter = Trick.Woodcutter || [];

  const Follow = opposite(Lead);

  if (Trick[Trick.Lead] === undefined) {
    return;
  }
  let foxIndex = 0;
  let woodcutterIndex = 0;
  if (Trick[Lead].Rank === 3 ) {
    if (Trick.Fox.length === foxIndex) {
      return "Fox";
    }
    foxIndex += 1;
  }
  if (Trick[Lead].Rank === 5) {
    if (Trick.Woodcutter.length === woodcutterIndex) {
      return "Woodcutter";
    }
    woodcutterIndex += 1;
  }

  if (Trick[Follow] === undefined) {
    return;
  }

  if (Trick[Follow].Rank === 3 ) {
    if (Trick.Fox.length === foxIndex) {
      return "Fox";
    }
  }
  if (Trick[Follow].Rank === 5) {
    if (Trick.Woodcutter.length === woodcutterIndex) {
      return "Woodcutter";
    }
  }
}
