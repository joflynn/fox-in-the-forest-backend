module.exports = function handVoid(Hand, Lead) {
  return Hand.findIndex(card => card.Suit === Lead.Suit) === -1;
}

