module.exports = function hasCard(Hand, Card) {
  if (Card === undefined) {
    return false; 
  }
  return Hand.findIndex(c => c.Rank === Card.Rank && c.Suit === Card.Suit) !== -1;
}
