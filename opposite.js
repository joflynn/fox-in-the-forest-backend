module.exports = function opposite(Side) {
  if (Side === "North") {
    return "South";
  }
  if (Side === "South") {
    return "North";
  }
}
