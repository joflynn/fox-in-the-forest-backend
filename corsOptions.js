const whitelist = [
  "http://localhost:3000",
  "http://fitf.s3-website-us-west-2.amazonaws.com", 
  "https://fitf.s3-website-us-west-2.amazonaws.com"
];

module.exports = {
  origin: function (origin, callback) {
    if ( whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  }
}

