const opposite = require("./opposite");

module.exports = function trickTurn(trick) {
  const { Lead } = trick;
  if (trick[Lead].Rank === undefined || trick[Lead].Suit === undefined) {
    return Lead;
  }

  if (trick[Lead].Rank === 3 && trick.Fox.length === 0) {
    return Lead;
  }
  if (trick[Lead].Rank === 5 && trick.Woodcutter.length === 0) {
    return Lead;
  }

  return opposite(Lead);
}
