module.exports = function trickDecree(Trick) {

  let Decree = Trick.Decree;
  if (Trick.Fox.length > 0) {
    Trick.Fox.forEach(fox => {
      Decree = fox.NewDecree;
    });
  }
  return Decree;
}
