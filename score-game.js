const scoreRound = require("./score-round");

module.exports = function scoreGame(game) {

  const Score = { North: 0, South: 0 };
  game.Rounds.forEach(round => {
    const { RoundScore } = scoreRound(round);
    Score.North += RoundScore.North;
    Score.South += RoundScore.South;
  });
  return Score;
}
