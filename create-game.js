const crypto = require("crypto");
const base64url = require("base64url");

module.exports = function createGame(game, request) {

//  const { Name, Length } = request;

  const north = base64url(crypto.randomBytes(64));
  const invite = base64url(crypto.randomBytes(64));

  //game.Name = Name;
  //game.Length = Length;
  game.North = north;
  game.Invite = invite;
  game.CreatedAt = new Date();
  return game;
};
