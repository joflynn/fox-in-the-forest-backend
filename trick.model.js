const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Trick = new Schema({
  _round: {
    type: Schema.Types.ObjectId, ref: "Round"
  },
  Index: Number,
  Lead: String,
  Decree: {Rank: Number, Suit: String},
  North: {Rank: Number, Suit: String},
  South: {Rank: Number, Suit: String},
  Fox: [{NewDecree: {Rank: Number, Suit: String}}],
  Woodcutter: [{Discard: {Rank: Number, Suit: String}}]
});

module.exports = mongoose.model("Trick", Trick);
