const trickWinner = require("./trick-winner");

const scoreMap = {
  0: 6,
  1: 6,
  2: 6,
  3: 6,
  4: 1,
  5: 2,
  6: 3,
  7: 6,
  8: 6,
  9: 6,
  10: 0,
  11: 0,
  12: 0,
  13: 0
};

const messageMap = {
  0: "Humble",
  1: "Humble",
  2: "Humble",
  3: "Humble",
  4: "Defeated",
  5: "Defeated",
  6: "Defeated",
  7: "Victorious",
  8: "Victorious",
  9: "Victorious",
  10: "Greedy",
  11: "Greedy",
  12: "Greedy",
  13: "Greedy"
};


module.exports = function scoreRound(Round) {
  const RoundScore = {North: 0, South: 0};
  const Tricks = {North: 0, South: 0};
  let Message = "";
  Round.Tricks.forEach(trick => {
    const Winner = trickWinner(trick);
    if (Winner !== undefined) {
      let points = 0;
      if (trick.North.Rank === 7) {
        points += 1;
      }
      if (trick.South.Rank === 7) {
        points += 1;
      }

      Tricks[Winner] += 1;
      RoundScore[Winner] += points;
    }
  });
  if (Round.Tricks.length === 13) {
    const LastTrick = Round.Tricks.slice(-1)[0];
    if (LastTrick.North.Rank !== undefined && LastTrick.South.Rank !== undefined) {
      RoundScore.North += scoreMap[Tricks.North];
      RoundScore.South += scoreMap[Tricks.South];

      Message = `North was ${messageMap[Tricks.North]}(${scoreMap[Tricks.North]}), South was ${messageMap[Tricks.South]}(${scoreMap[Tricks.South]}).`;
    }
  }
  return {RoundScore, Tricks, Message};
}
