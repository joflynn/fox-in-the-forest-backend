const crypto = require("crypto");
const base64url = require("base64url");
const Round = require("./round.model");
const Trick = require("./trick.model");

const dealRound = require("./deal-round");

module.exports = function joinGame(game) {
  const south = base64url(crypto.randomBytes(64));
  game.South = south;
  game.Invite = null;

  const round = new Round();
  round.Index = 1;
  round.Dealer = "North";
  round.Seed = base64url(crypto.randomBytes(64));

  const deal = dealRound(round);
  const trick = new Trick();
  trick.Index = 1;
  trick.Lead = "South";
  trick.Decree = deal.Decree;
  round.Tricks.push(trick);
  game.Rounds.push(round);
  game.UpdatedAt = new Date();
  return game;
};
