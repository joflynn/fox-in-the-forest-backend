const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");

const PORT = process.env.PORT || 4000;

const corsOptions = require("./corsOptions");

const app = express();
app.use(cors(corsOptions));
app.use(bodyParser.json());

mongoose.connect(process.env.MONGODB_URI || "mongodb://127.0.0.1:27017/fitf", {
  useNewUrlParser: true
});
const connection = mongoose.connection;

connection.once("open", () => {
  console.log("connected to db");
});

app.listen(PORT, () => {
  console.log("server started");
});

const fitfRoutes = express.Router();
app.use("/fitf", fitfRoutes);


const gameSide = require("./game-side");
const createGame = require("./create-game");
const joinGame = require("./join-game");
const dealRound = require("./deal-round");
const gameNotice = require("./game-notice");
const trickTurn = require("./trick-turn");
const trickSpecial = require("./trick-special");
const trickWinner = require("./trick-winner");
const advanceGame = require("./advance-game");
const scoreRound = require("./score-round");
const scoreGame = require("./score-game");
const opposite = require("./opposite");
const hasCard = require("./has-card");
const handVoid = require("./hand-void");
const monarchRequires = require("./monarch-requires");

const Game = require("./game.model");
const Round = require("./round.model");
const Trick = require("./trick.model");

fitfRoutes.route("/games/create").post((req, res) => {
  const game = createGame(new Game(req.body));

  game.save().then(game => {
    const { Invite, Name, Length } = game;
    const Key = game.North;
    const Id = game._id;
    res.status(200).json({ Id, Invite, Key, Name, Length });
  }).catch(err => {
    res.status(500).json(err);
  });
});

fitfRoutes.route("/games/check/:Invite").get((req, res) => {
  const { Invite } = req.params;
  Game.findOne({ Invite }).exec((err, game) => {
    if (game !== null) {
      const { Name, Length, CreatedAt } = game;
      const Id = game._id;
      res.status(200).json({ Id, Name, Length });
    } else {
      res.status(404).send("Not Found");
    }
  });
});

fitfRoutes.route("/games/join/:Invite").put((req, res) => {
  const { Invite } = req.params;
  Game.findOne({ Invite }).exec((err, game) => {
    if (game !== null) {
      const updated = joinGame(game);
      updated.save().then(game => {
        const { Name, CreatedAt } = game;
        const Key = game.South;
        const Id = game._id;
        res.status(200).json({ Id, Name, Key, CreatedAt });
      }).catch(err => {
        res.status(500).json(err);
      });
    } else {
      res.status(404).send("Not Found");
    }
  });
});

fitfRoutes.route("/game/:Id/:Key").get((req, res) => {
  const { Id, Key } = req.params;
  Game.findOne({ _id: Id}).exec((err, game) => {
    if (game !== null) {
      const Side = gameSide(game, Key);
      let Status = "";
      let response = {};
      const { Name, Length, Invite } = game;
      if (game.Invite) {
        Status = "Setup";
        response = {Name, Length, Invite, Status};
      } else { 
        const Round = game.Rounds.slice(-1)[0];
        const LastRound = game.Rounds.slice(-2)[0];
        const Trick = Round.Tricks.slice(-1)[0];
        const deal = dealRound(Round, Side);
        const Turn = trickTurn(Trick);
        const Special = trickSpecial(Trick);
        const { Hand, Decree } = deal;
        const { RoundScore, Tricks } = scoreRound((Round.Tricks.length === 1) ? LastRound : Round);
        const Score = scoreGame(game);
        let LastTrick;
        if (Trick.Index > 1) {
          LastTrick = Round.Tricks.slice(-2)[0];
        }

        Status = `Round ${Round.Index} - Trick ${Trick.Index}`;
        Notice = gameNotice(game);

        response = { Name, Length, Round, Status, Notice, Trick, LastTrick, Turn, Special, RoundScore, Score, Side, Decree, Tricks, Hand };
      }
      res.status(200).json(response);
    } else {
      res.status(404).send("Not Found");
    }
  });
});

fitfRoutes.route("/game/:Id/play").post((req, res) => {
  const { Id } = req.params;
  const { Key, Card } = req.body;
  Game.findOne({ _id: Id }).exec((err, game) => {
    if (game !== null) {
      const Side = gameSide(game, Key);
      const Round = game.Rounds.slice(-1)[0];
      const deal = dealRound(Round, Side);
      const trick = Round.Tricks.slice(-1)[0];
      const Turn = trickTurn(trick);
      if (Turn === Side 
       && hasCard(deal.Hand, Card) ) {
        if (Side === trick.Lead || Card.Suit === trick[trick.Lead].Suit || handVoid(deal.Hand, trick[trick.Lead])) { 
          const monarchCard = monarchRequires(deal.Hand, trick[trick.Lead]);
          if (Side !== trick.Lead && trick[trick.Lead].Rank === 11 && (Card.Rank !== monarchCard.Rank || Card.Suit !== monarchCard.Suit)) {
            res.status(401).send(`Monarch Requires ${monarchCard.Rank} ${monarchCard.Suit}`);
          } else {
            trick[Side] = Card;
            const response = {
              Status: `${opposite(Side)} To Follow`
            };
            if (Card.Rank === 3 ) {
              response.Status = "May Change Decree";
              response.Decree = trick.Decree;
            }
            if (Card.Rank === 5) {
              response.Status = "Woodcutter Draw";
              response.Card = deal.draw();
            }

            game = advanceGame(game);
            game.save().then(game => {
              res.status(200).json(response);
            }).catch(err => {
              res.status(500).json(err);
            });
          }
        } else {
          res.status(401).send("Must Follow Suit");
        }
      } else {
        res.status(401).send("Unauthorized");
      }
    } else {
      res.status(404).send("Not Found");
    }
  });
});


fitfRoutes.route("/game/:Id/fox").post((req, res) => {
  const { Id } = req.params;
  const { Key, NewDecree } = req.body;

  Game.findOne({ _id: Id}).exec((err, game) => {
    if (game !== null) {
      const Side = gameSide(game, Key);
      const Round = game.Rounds.slice(-1)[0];
      const deal = dealRound(Round, Side);
      const trick = Round.Tricks.slice(-1)[0];
      if (trick[Side] !== undefined && trick[Side].Rank === 3 && hasCard(deal.Hand, NewDecree)) {
        trick.Fox.push({NewDecree});

        game = advanceGame(game);
        game.save().then(game => {
          res.status(200).json({Status: "Decree Changed"});
        }).catch(err => {
          res.status(500).json(err);
        });
      } else {
        res.status(401).send("Unauthorized");
      }
    } else {
      res.status(404).send("Not Found");
    }
  });
});

fitfRoutes.route("/game/:Id/woodcutter").post((req, res) => {
  const { Id } = req.params;
  const { Key, Discard } = req.body;

  Game.findOne({ _id: Id}).exec((err, game) => {
    if (game !== null) {
      const Side = gameSide(game, Key);
      const Round = game.Rounds.slice(-1)[0];
      const deal = dealRound(Round, Side);
      const trick = Round.Tricks.slice(-1)[0];
      
      if (trick[Side] !== undefined && trick[Side].Rank === 5 && hasCard(deal.Hand, Discard)) { 
        trick.Woodcutter.push({Discard});

        game = advanceGame(game);
        game.save().then(game => {
          res.status(200).json({Status: "Discarded"});
        }).catch(err => {
          res.status(500).json(err);
        });
      } else {
        res.status(401).send("Unauthorized");
      }
    } else {
      res.status(404).send("Not Found");
    }
  });
});

